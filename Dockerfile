FROM eclipse-temurin:latest as builder

WORKDIR /app

COPY build.gradle .
COPY settings.gradle .
COPY src ./src
COPY /gradle ./gradle
COPY gradlew .

RUN ./gradlew build -x check

FROM eclipse-temurin:latest as runner

COPY --from=builder /app/build/libs/spring_authorization.jar app.jar

CMD ["java", "-jar" ,"app.jar"]
