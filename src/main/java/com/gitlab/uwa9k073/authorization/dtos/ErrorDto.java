package com.gitlab.uwa9k073.authorization.dtos;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorDto {

  private String message;
}
