package com.gitlab.uwa9k073.authorization.dtos;


import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class SignUpDto {

  private String nickname;
  private String phone;
  private char[] password;
}
