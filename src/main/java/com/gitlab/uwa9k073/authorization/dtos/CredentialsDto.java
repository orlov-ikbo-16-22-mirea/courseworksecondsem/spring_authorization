package com.gitlab.uwa9k073.authorization.dtos;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CredentialsDto {

  private String phone;
  private char[] password;
}
