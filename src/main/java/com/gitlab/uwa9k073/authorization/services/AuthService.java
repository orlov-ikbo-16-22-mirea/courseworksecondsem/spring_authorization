package com.gitlab.uwa9k073.authorization.services;

import com.gitlab.uwa9k073.authorization.dtos.CredentialsDto;
import com.gitlab.uwa9k073.authorization.dtos.SignUpDto;
import com.gitlab.uwa9k073.authorization.dtos.TokenDto;
import java.util.UUID;
import org.springframework.data.util.Pair;

public interface AuthService {

  Pair<TokenDto, TokenDto> signUp(SignUpDto signUpDto, UUID idempotencyKey)
      throws IllegalAccessException;

  Pair<TokenDto, TokenDto> signIn(CredentialsDto credentialsDto) throws IllegalAccessException;

  TokenDto reissueAccessToken(String refreshToken) throws IllegalAccessException;

}
