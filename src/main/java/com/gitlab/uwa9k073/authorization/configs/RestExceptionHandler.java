package com.gitlab.uwa9k073.authorization.configs;

import com.gitlab.uwa9k073.authorization.dtos.ErrorDto;
import com.gitlab.uwa9k073.authorization.exceptions.AppException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class RestExceptionHandler {

  @ExceptionHandler(value = {AppException.class})
  @ResponseBody
  public ResponseEntity<ErrorDto> handleException(AppException ex) {
    return ResponseEntity
        .status(ex.getStatus())
        .body(new ErrorDto(ex.getMessage()));
  }
}
