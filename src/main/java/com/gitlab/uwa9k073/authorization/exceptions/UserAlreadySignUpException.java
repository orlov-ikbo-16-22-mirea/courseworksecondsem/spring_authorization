package com.gitlab.uwa9k073.authorization.exceptions;

public class UserAlreadySignUpException extends RuntimeException {

  public UserAlreadySignUpException(String message) {
    super(message);
  }
}
