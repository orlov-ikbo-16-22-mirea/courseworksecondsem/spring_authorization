package com.gitlab.uwa9k073.authorization.exceptions;

public class InvalidPasswordException extends RuntimeException {

  public InvalidPasswordException(String message) {
    super(message);
  }
}
