package com.gitlab.uwa9k073.authorization.mappers;

import com.gitlab.uwa9k073.authorization.dtos.SignUpDto;
import com.gitlab.uwa9k073.authorization.entities.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

  public User toUser(SignUpDto user) {
    return User.builder().nickname(user.getNickname()).phone(user.getPhone()).build();
  }

}
