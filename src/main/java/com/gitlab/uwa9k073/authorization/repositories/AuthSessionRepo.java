package com.gitlab.uwa9k073.authorization.repositories;

import com.gitlab.uwa9k073.authorization.entities.AuthSession;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthSessionRepo extends JpaRepository<AuthSession, UUID> {

  Optional<AuthSession> findAuthSessionByRefreshToken(String refreshToken);
}
