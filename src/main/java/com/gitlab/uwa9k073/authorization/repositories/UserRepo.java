package com.gitlab.uwa9k073.authorization.repositories;

import com.gitlab.uwa9k073.authorization.entities.User;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends JpaRepository<User, UUID> {

  Optional<User> findByPhone(String phone);
}
